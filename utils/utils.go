package utils

import (
	"math/rand"
	"time"

	"golang.org/x/exp/constraints"
)

func Max[T constraints.Ordered](args ...T) T {
	if len(args) == 0 {
		return *new(T)
	}
	max := args[0]
	for _, arg := range args[1:] {
		if arg > max {
			max = arg
		}
	}
	return max
}

func Min[T constraints.Ordered](args ...T) T {
	if len(args) == 0 {
		return *new(T)
	}
	min := args[0]
	for _, arg := range args[1:] {
		if arg < min {
			min = arg
		}
	}
	return min
}

func initRand() {
	rand.Seed(time.Now().UnixNano()) // seed random
}

func RandomInt64(min, max int64) int64 {
	initRand()
	return min + rand.Int63n(max-min+1)
}

func D100() int {
	// random number from 0 to 100
	return int(RandomInt64(0, 100))
}
