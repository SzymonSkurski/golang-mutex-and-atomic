module gitlab.com/SzymonSkurski/golang-mutex-and-atomic

go 1.19

require (
	gitlab.com/SzymonSkurski/prettyprinter v0.0.0-20230414151341-bf55aab56248
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29
)

require github.com/TwiN/go-color v1.4.0 // indirect
