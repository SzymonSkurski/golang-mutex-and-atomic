package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/SzymonSkurski/golang-mutex-and-atomic/utils"
	"gitlab.com/SzymonSkurski/prettyprinter"
)

const (
	baseHP     = 1000
	turns      = 100
	difficulty = 2 // from 1 to 10
)

type Player struct {
	mu     sync.RWMutex // mutex allow to temporary lock state
	HP     int64        // player's health
	Wounds int64
}

func NewPlayer(hp int64) *Player {
	return &Player{
		// mu: sync.RWMutex{},
		HP: hp,
	}
}

func (p *Player) getHP() int64 {
	// use atomic to read hp
	return atomic.LoadInt64(&p.HP)
}

func (p *Player) getWounds() int64 {
	// use atomic to read hp
	return atomic.LoadInt64(&p.Wounds)
}

func (p *Player) getHealth() int64 {
	// use atomic to read hp
	HP := p.getHP()
	w := p.getWounds()
	return HP - w
}

func (p *Player) addWounds(w int64) {
	// use mutex to lock
	p.mu.Lock()
	defer p.mu.Unlock()                                  // unlock at the function end
	p.Wounds = utils.Min(utils.Max(0, p.Wounds+w), p.HP) // avoid HP below zero or above HP
}

func (p *Player) IsDead() bool {
	return p.getHealth() == 0
}

func randomDamage(p *Player) {
	d := utils.Max(1, utils.Min(10, difficulty)) // keep range 1 - 10
	damage := int64(utils.D100()) + int64((d-1)*10)
	p.addWounds(damage)
}

func randomHeal(p *Player) {
	d := int64(utils.D100())
	p.addWounds(-d)
}

func startGUILoop(p *Player) {
	ticker := time.NewTicker(time.Millisecond * 200)
	for {
		fmt.Print("\033[H\033[2J") // clear screen
		// prettyprinter.PP(p)
		h := p.getHealth()
		l := fmt.Sprintf("Player health:%d", h)
		switch {
		// below 25 %
		case h < baseHP/4:
			prettyprinter.PrintRed(l)
		// below 50 %
		case h < baseHP/2:
			prettyprinter.PrintYellow(l)
		default:
			prettyprinter.PrintGreen(l)
		}
		<-ticker.C // wait for tic
	}
}

func startLogicLoop(p *Player) {
	c := 0
	ticker := time.NewTicker(time.Millisecond * 200)
	for {
		randomHeal(p)
		randomDamage(p)
		if p.IsDead() {
			fmt.Println("player DIED after ", c, " turns")
			break
		}
		if c >= turns {
			fmt.Println("You Won ! Survived ", c, " turns")
			break
		}
		c++
		<-ticker.C // wait for next tic
	}
}

func main() {
	player := NewPlayer(baseHP)
	go startGUILoop(player)
	startLogicLoop(player)
}

// go test ./... --race check for risk of simultaneously write and read
